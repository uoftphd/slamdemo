//  common.h
//  Created by Ray Lo on 2014-07-26.

#ifndef _COMMON_h
#define _COMMON_h

//standard c++ libraries
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

using namespace std;

//include for all operations
#include <opencv2/opencv.hpp>
using namespace cv;

#define FOV 0.7243116

#endif
