/*
 * MPV.cpp
 *
 *  Created on: Nov 30, 2017
 *      Author: raymond
 */

#include "MVP.h"

//by default it is just indentity matrix.
MVP::MVP(){
	mvp_m = glm::mat4(1.0f);
	view_m = glm::mat4(1.0f);
	projective_m = glm::mat4(1.0f);
	model_m = glm::mat4(1.0f);
	height = 1280;
	width = 720;
	fov = 0.5f;
	near_plane = 0.01f;
	far_plane = 100.0f;
}

MVP::MVP(float i_fov, float i_near_plane, float i_far_plane) {
	//fixed for the current headset, should be reset
	near_plane = i_near_plane;
	far_plane = i_far_plane;
	fov = i_fov;

	//setup t
	pose_rotation = glm::mat4(1.0f);
	pose_translation = glm::vec3(0.0f);
	model_m = glm::mat4(1.0f);
}

MVP::~MVP() {
	// TODO Auto-generated destructor stub
}

void MVP::setPoseRotationMatrix(glm::mat4 pose_matrix_camera){
	glm::vec3 euler = glm::eulerAngles(glm::quat_cast(pose_matrix_camera));
	//inversing the axis and drrections
	pose_rotation = glm::eulerAngleXYZ(-euler[0], euler[1], euler[2]);
}

void MVP::setPoseTranslationMatrix(glm::vec3 translation){
	pose_translation = translation;
}

void MVP::setProjectiveMatrixParam(float i_fov, float i_width, float i_height){
	this->fov = i_fov;
	this->height = i_height;
	this->width = i_width;
}

glm::mat4 MVP::getMVP(){
	return mvp_m;
}

void MVP::setModelMatrix(glm::mat4 model){
	this->model_m = model;
}
//this will get the MVP as well as compute the MPV
glm::mat4 MVP::computeMVP(){
	//compute the perspective matrix (in case we updated the parameters of the FOV and so in run time
	projective_m = glm::perspective(fov,
				(float) width / (float) height, near_plane, far_plane);

	//compute the new view matrix pose based on the camera pose

	//TODO: should add a new intrinsic for the display position here - now hard coded in
	glm::vec3 eye_pos = glm::vec3(pose_translation[0]-0.12f,
		-pose_translation[1], -pose_translation[2]); //world to view matrix (eye position in space)
	glm::vec4 dir = glm::vec4(0.0f, 0.0f, -1.0f, 1.0f); //the viewing direction
	dir = pose_rotation * dir;
	glm::vec4 up_vec = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	up_vec = pose_rotation * up_vec;
	view_m = glm::lookAt(eye_pos, eye_pos + glm::vec3(dir.x, dir.y, dir.z),
			glm::vec3(up_vec.x, up_vec.y, up_vec.z));

	//gets the View and Model Matrix and apply to the rendering
	mvp_m = projective_m*view_m*model_m;
	return mvp_m;
}
