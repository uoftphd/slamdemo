/*
 * OBJRenderer.cpp
 *
 *  Created on: Nov 28, 2017
 *      Author: raymond
 */

#include "OBJRenderer.h"

OBJRenderer::OBJRenderer() {
	// TODO Auto-generated constructor stub
	alpha_v = 1.0f;
}

OBJRenderer::~OBJRenderer() {
	// TODO Auto-generated destructor stub
}

void OBJRenderer::end(){
	glDisableVertexAttribArray(attribute_vertex);
	glDeleteVertexArrays(1, &vertex_array_id);

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertex_buffer);
	glDeleteProgram(program_id);

	free(g_vertex_buffer_data);
}
void OBJRenderer::updatePose(glm::mat4 pose) {
	//pose_matrix = pose;
	mvp.setPoseRotationMatrix(pose);
}
void OBJRenderer::updateModel(glm::mat4 pose){
	mvp.setModelMatrix(pose);
}
void OBJRenderer::updateTranslation(glm::vec3 translation) {
	//pose_translation = translation;
	mvp.setPoseTranslationMatrix(translation);
}
void OBJRenderer::setAlpha (float alpha){
	this->alpha_v = alpha;
}

void OBJRenderer::render(int width, int height) {
	//Now perform overlays
	glUseProgram(program_id);
	//1st attribute buffer : vertices for position
	glEnableVertexAttribArray(attribute_vertex);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glVertexAttribPointer(attribute_vertex, 3, GL_FLOAT, GL_FALSE, 0,
			(void*) 0);


	mvp.setProjectiveMatrixParam(FOV, width, height);

	glm::mat4 mvp_m = mvp.computeMVP();

	//sends our transformation to the currently bound shader,
	//in the "MVP" uniform variable
	glUniformMatrix4fv(matrix_id, 1, GL_FALSE, &mvp_m[0][0]);
	glUniform1f(alpha_id, alpha_v);

	//render scene, with different mode, and can be enabled separately to get different effects
	//if (drawTriangles) {
	//obj_loader->draw(GL_TRIANGLES);
	//}
	//if (drawPoints) {
	//obj_loader->draw(GL_POINTS);
	//glDrawArrays(GL_TRIANGLES, 0, numVerticesScene);
	//glDrawArrays(GL_POINTS, 0, numVerticesScene);
	glDrawArrays(GL_LINES, 0, numVerticesScene);

	//}
	//if (drawLines) {
	//obj_loader->draw(GL_LINES);
	//}

	//swap buffers
}
void OBJRenderer::init(string filename) {
	//Shader Program for 3D Objects
	program_id = 0;
	matrix_id = 0;
	alpha_id = 0;
	attribute_vertex = 0;
	vertex_array_id = 0;
	vertex_buffer = 0;

	////////////////////////////////////////////////////////////////////////////////////
	//For 3D Overlay Side
	program_id = LoadShaders("data/pointcloud.vert", "data/pointcloud.frag");

	ObjLoader *obj_loader = new ObjLoader();
	int result = 0;
	obj_loader->setScale(0.15f);
	result = obj_loader->loadAsset(filename.c_str());
	//obj_loader->setScale(0.25);

	if (result) {
		fprintf(stderr, "Final to Load the 3D file\n");
		//exit(EXIT_FAILURE);
	}

	//get the location for our "MVP" uniform variable
	matrix_id = glGetUniformLocation(program_id, "MVP");
	alpha_id = glGetUniformLocation(program_id, "alpha");

	//uses a large buffer to store the entire scene
	g_vertex_buffer_data = (GLfloat*) malloc(
			obj_loader->getNumVertices() * sizeof(GLfloat));
	numVerticesScene = obj_loader->getNumVertices();
	//load the scene into the vertex buffer
	obj_loader->loadVertices(g_vertex_buffer_data);

	//get the location for the attribute variables
	attribute_vertex = glGetAttribLocation(program_id,
			"vertexPosition_modelspace");

	//generate the Vertex Array Object (Depedency: GLEW)
	glGenVertexArrays(1, &vertex_array_id);
	glBindVertexArray(vertex_array_id);

	//initialize the vertex buffer memory (similar to malloc)
	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	//load data
	glBufferData(GL_ARRAY_BUFFER,
			obj_loader->getNumVertices() * sizeof(GLfloat),
			g_vertex_buffer_data, GL_STATIC_DRAW);

	//uses our shader
	glUseProgram(program_id);

	//1st attribute buffer : vertices for position
	glEnableVertexAttribArray(attribute_vertex);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glVertexAttribPointer(attribute_vertex, 3, GL_FLOAT, GL_FALSE, 0,
			(void*) 0);
}
