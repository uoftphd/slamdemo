/*
 * VoiceBubble.h
 * Voice Bubble that stay in the real world.
 *  Created on: Nov 27, 2017
 *      Author: raymond
 */

#ifndef SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_VOICEBUBBLE_H_
#define SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_VOICEBUBBLE_H_

#include <string>
#include <vector>

//use opencv for basic drawing and all
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <AL/al.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

using namespace std;
class VoiceBubble {
private:
	//should create a recording object
    ALbyte *voice_data;
	std::size_t voice_size;
    std::size_t current_size;

	std::vector<std::string> transcribed_text;
	std::vector<float> transcribed_confidence;

	unsigned int clipsize;
	std::string format;
	cv::Mat bubbleTexture;
	cv::Mat defaultTexture;

	glm::vec3 position;
	glm::mat4 pose;

public:
	void appendAudioBuffer(ALbyte *data, size_t size);
	cv::Mat getBubble();

	//all text related items
	void clearText();
	void appendText(std::string input_text, float confidence);
	void drawText();

	unsigned short *get_voice_recording;
	VoiceBubble();
	virtual ~VoiceBubble();
};

#endif /* SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_VOICEBUBBLE_H_ */
