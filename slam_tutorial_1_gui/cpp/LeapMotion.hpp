#ifndef LEAPMOTION_HPP
#define LEAPMOTION_HPP

#include <iostream>
#include <cstring>
#include "Leap.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace Leap;

class SampleListener : public Listener {
private:
	glm::vec3 palm_angles;
	glm::vec3 palm_position;
	int hand_count;
public:
    virtual void onInit(const Controller&);
    virtual void onConnect(const Controller&);
    virtual void onDisconnect(const Controller&);
    virtual void onExit(const Controller&);
    virtual void onFrame(const Controller&);
    virtual void onFocusGained(const Controller&);
    virtual void onFocusLost(const Controller&);
    virtual void onDeviceChange(const Controller&);
    virtual void onServiceConnect(const Controller&);
    virtual void onServiceDisconnect(const Controller&);
    virtual void onServiceChange(const Controller&);
    virtual void onDeviceFailure(const Controller&);
    virtual void onDiagnosticEvent(const Controller&, const char*);
    glm::vec3 getPalmAngles(){return palm_angles;}
    glm::vec3 getPalmPosition(){return palm_position;}
    int getHandCount(){return hand_count;}
};

class LeapMotionController{
private:
	  SampleListener listener;
	  Controller controller;

public:
	  LeapMotionController();
	  void run();
	  void end();
	  glm::vec3 getPalmAngles(){return listener.getPalmAngles();}
	  glm::vec3 getPalmPosition(){return listener.getPalmPosition();}
	  int getHandCount(){return listener.getHandCount();}
	  ~LeapMotionController();
};


#endif
