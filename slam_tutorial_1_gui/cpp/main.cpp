// License: Apache 2.0. See LICENSE file in root directory.
// Copyright(c) 2017 Intel Corporation. All Rights Reserved.

//
// slam_tutorial_1_gui: This app illustrates the use of the librealsense and librealsense_slam libraries.
// The librealsense library streams depth, fish eye, and IMU data from the ZR300 camera which are used
// as input to the librealsense_slam library. The librealsense_slam library outputs the camera pose
// (position and orientation) and occupancy map. The occupancy map is a 2D map of the surroundings
// that shows which areas are occupied by obstacles to navigation.
//
//force the application to compile with radians measure only.

#define GLM_FORCE_RADIANS
//#define VIDEO_DEMO

#include <iostream>
#include <iomanip>
#include <map>
#include <thread>
#include <memory>
#include <vector>

//Tracking Module
//SLAM by IntelRealSense is used for this example
#include <librealsense/rs.hpp>
#include <rs_sdk.h>
#include <librealsense/slam/slam.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "version.h"
#include "slam_utils.hpp"

//Rendering Manager
#include "RendererManager.h"

//Leap Motion
#include "LeapMotion.hpp"

//Google API
#include "GoogleVoiceDemo.h"

//Voice Bubble
#include "VoiceBubble.h"

//Multi-threaded to enable different interactions (e.g., voice bubble)
#include <thread>
#include <mutex>

using namespace std;
using namespace rs::core;
using namespace rs::slam;
//using namespace cv;

const std::string WINDOW_NAME = "MODAR Debug";
const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;

//GFLW Windows Manager shared variable (including the
//key stroke variables
GLFWwindow* g_window;
int WINDOWS_WIDTH=1280;
int WINDOWS_HEIGHT=720;
const int DEPTH_WIDTH = 320;
const int DEPTH_HEIGHT = 240;
const int FISH_WIDTH = 640;
const int FISH_HEIGHT = 480;

bool drawLines=true;
bool drawPoints=true;
bool drawTriangles=true;
bool drawBackground = true;
bool reset=false;
bool debug_depth = true;

//Instances of the variables lives in this demo
RendererManager *rendererM;
//play with 3 bubbles for now
VoiceBubble *voice_bubble;
vector <VoiceBubble*> bubble_list;
GoogleVoiceDemo *google_voice;
LeapMotionController *leapM;

//For SLAM
glm::vec3 pose_translate;
glm::mat4 pose_matrix=glm::mat4( 1.0 );
float alpha_v = 0.2f;
float depth_scale = 1.0f;
uint8_t *fish_buffer = NULL;
Mat depth_cv(Size(DEPTH_WIDTH, DEPTH_HEIGHT), CV_16UC1);
Mat output (Size(DEPTH_WIDTH, DEPTH_HEIGHT), CV_8UC3);
// Version number of the samples
//extern constexpr auto rs_sample_version = concat("VERSION: ",RS_SAMPLE_VERSION_STR);

//--------------------------------------------------------------------------------------
// This event handler receives updates from the SLAM module
//--------------------------------------------------------------------------------------
static void key_callback(GLFWwindow* window, int key,
		int scancode, int action, int mods) {
	if (action == GLFW_PRESS) {
		switch (key) {
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, GL_TRUE);
			break;
		case GLFW_KEY_SPACE:
			reset=!reset;
			break;
		case GLFW_KEY_3:
			break;
		case GLFW_KEY_4:
			break;
		case GLFW_KEY_5:
			break;
		case GLFW_KEY_R:
			break;
		case GLFW_KEY_L:
			break;
		case GLFW_KEY_0:
			drawBackground=!drawBackground;
			break;
		case GLFW_KEY_A:
			//run the google voice, and draw on the bubble
			//voice_bubble->drawText("Hello World Hello World");
			//reset position
			google_voice->extract(voice_bubble); //this will terminate the older thread if it was running
			break;
		case GLFW_KEY_B:
			//voice_bubble->drawText("Hello World Hello World Hello World");
			break;
		case GLFW_KEY_C:
			voice_bubble->clearText();
			//voice_bubble->drawText("Hello World Hello World Hello World Hello World Hello World Hello World");
			break;
		case GLFW_KEY_D:
			debug_depth = !debug_depth;
			rendererM->setDebug(debug_depth);
			break;
		default:
			break;
		}
	}
}
class slam_event_handler: public rs::core::video_module_interface::processing_event_handler {
	//std::shared_ptr<occupancy_map> occ_map;
public:

	//NOTE: must keep this loop very low weight or it will block the pose updates
	void module_output_ready(rs::core::video_module_interface * sender,
			correlated_sample_set * sample) {
		//rs::core::status status;
		// Get a handle to the slam module
		auto slam = dynamic_cast<rs::slam::slam*>(sender);

		// Create an occupancy map container if we don't already have one.
		//if (!occ_map)
		//{
		// This will only happen on the first execution of this callback.
		//	occ_map = slam->create_occupancy_map(50 * 50);
		//}
		//handle some slam related pieces
//		if(reset_pose){
//			printf("Loading the maps\n");
//			reset_pose = !reset_pose;
//			//slam->start_relocalization_mapping();
//			status = slam->load_occupancy_map("occupancy_map.bin");
//			if(status == status_no_error){
//				printf("Loaded Occupancy Map Successfully \n");
//			}else{
//				printf("Load Occupancy Map Failed \n");
//			}
//		}
		// Get the number of occupancy map tiles that were updated
		//slam->get_occupancy_map_update(occ_map);
		//int count_updated_tiles = occ_map->get_tile_count(); // This will only be > 0 when the tracking accuracy is > low.

		// Print the camera pose and number of updated occupancy map tiles to the console
		//cout << fixed << setprecision(4) << "Translation:(X=" << pose.m_data[3] << ", Y=" << pose.m_data[7] << ", Z=" << pose.m_data[11] << ")    Accuracy:" << trackingAccuracy << "    Tiles_Updated:" << count_updated_tiles << "\r" << flush;

		// Get the occupany map (with trajectory) as an image and render it in the map window
//		unsigned char *map_image;
//		unsigned int width, height;
//		status = slam->get_occupancy_map_as_rgba(&map_image, &width, &height, true, true);
//		if (status == status_no_error)
//		{
//			Mat map(height, width, CV_8UC4, map_image, Mat::AUTO_STEP);
//		//	render_image(WINDOW_NAME, map_image, width, height);
//			cv::imshow(WINDOW_NAME, map);
//		}

		// 3. send fisheye
		//auto fisheye = sample->images[(int)rs::core::stream_type::fisheye];
		//auto fish_info = fisheye->query_info();
		//uint64_t micros = fisheye->query_time_stamp() * 1000.0*1000.0;
		//web_view->on_fisheye_frame(micros, fish_info.width, fish_info.height, fisheye->query_data());
//       Mat fish_cv(Size(fish_info.width, fish_info.height), CV_8UC1, (void*)fisheye->query_data(), Mat::AUTO_STEP);
//	    //cv::equalizeHist( depth_cv, depth_cv );
//	    //cv::applyColorMap(depth_cv, depth_cv, COLORMAP_JET);
//	    cv::imshow("FishEye", fish_cv);
//	    waitKey(1);

		auto depth = sample->images[(int) rs::stream::depth];
		if (depth && debug_depth) {
			auto depth_info = depth->query_info();
			memcpy((unsigned char*) depth_cv.data, depth->query_data(),
					sizeof(uint16_t) * depth_info.width * depth_info.height);
		}
		auto fisheye = sample->images[(int) rs::core::stream_type::fisheye];
		//uint64_t micros = fisheye->query_time_stamp() * 1000.0*1000.0;
		//web_view->on_fisheye_frame(micros, fish_info.width, fish_info.height, fisheye->query_data());
		if (fisheye && debug_depth) {
			if (fish_buffer == NULL) {
				fish_buffer = (uint8_t*) malloc(
						sizeof(uint8_t) * FISH_HEIGHT * FISH_WIDTH);
			}
			//auto fish_info = fisheye->query_info();
			memcpy(fish_buffer, fisheye->query_data(),
					sizeof(uint8_t) * FISH_HEIGHT * FISH_WIDTH);
		}
		//printf("I got %d %d", depth_info.width, depth_info.height);
		//uint64_t micros = fisheye->query_time_stamp() * 1000.0*1000.0;
//        //web_view->on_fisheye_frame(micros, fish_info.width, fish_info.height, fisheye->query_data());
		//Mat depth_cv(Size(depth_info.width, depth_info.height), CV_16U, (void*)depth->query_data(), Mat::AUTO_STEP);
		//cv::imshow("Depth", depth_cv);
		//waitKey(1);

//		if(save_pose){
//			printf("Saving the maps");
//			save_pose = !save_pose;
//			status = slam->save_relocalization_map("relocalization_map.bin");
//			if(status == status_no_error){
//				printf("Saved Relocalization Map Successfully \n");
//			}else{
//				printf("Saved Relocalization Map Failed \n");
//			}
//			status = slam->save_occupancy_map("occupancy_map.bin");
//			if(status == status_no_error){
//				printf("Saved Occupancy Map Successfully \n");
//			}else{
//				printf("Saved Occupancy Map Failed \n");
//			}
//		}

		// Get the camera pose
		PoseMatrix4f pose;
		slam->get_camera_pose(pose);

		// Get the tracking accuracy as a string
		const std::string trackingAccuracy = tracking_accuracy_to_string(
				slam->get_tracking_accuracy());

		if (slam->get_tracking_accuracy() == tracking_accuracy::failed) {
			alpha_v = 0.01f;
		} else if (slam->get_tracking_accuracy() == tracking_accuracy::low) {
			alpha_v = 0.15f;
		} else if (slam->get_tracking_accuracy() == tracking_accuracy::medium) {
			alpha_v = 0.45f;
		} else if (slam->get_tracking_accuracy() == tracking_accuracy::high) {
			alpha_v = 0.65f;
		}
		//update the pose globally
		pose_translate[0] = pose.m_data[3];
		pose_translate[1] = pose.m_data[7];
		pose_translate[2] = pose.m_data[11];

		pose_matrix[0][0] = pose.m_data[0];
		pose_matrix[0][1] = pose.m_data[1];
		pose_matrix[0][2] = pose.m_data[2];
		pose_matrix[0][3] = 0.0f;	    //pose.m_data[3];
		pose_matrix[1][0] = pose.m_data[4];
		pose_matrix[1][1] = pose.m_data[5];
		pose_matrix[1][2] = pose.m_data[6];
		pose_matrix[1][3] = 0.0f;	    //pose.m_data[7];
		pose_matrix[2][0] = pose.m_data[8];
		pose_matrix[2][1] = pose.m_data[9];
		pose_matrix[2][2] = pose.m_data[10];
		pose_matrix[2][3] = 0.0f;	    //pose.m_data[11];
		pose_matrix[3][0] = pose.m_data[12];
		pose_matrix[3][1] = pose.m_data[13];
		pose_matrix[3][2] = pose.m_data[14];
		pose_matrix[3][3] = pose.m_data[15];

		//update the pose elements
		rendererM->updatePose(pose_matrix);
		rendererM->updateTranslation(pose_translate);
		rendererM->setAlpha(alpha_v);
	}
};

//Initialize the GLFW (the windows manager for OpenGL and handles some keyboard inputs).
void initGLFW(){
	//Initialize the GLFW
	if (!glfwInit()) {
		fprintf( stderr, "Failed to initialize GLFW\n");
		exit(EXIT_FAILURE);
	}
	printf("Initialized GLFW\n");

	//enable anti-alising 4x with GLFW
	glfwWindowHint(GLFW_SAMPLES, 8);
	//specify the client API version that the created context must be compatible with.
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	//make the GLFW forward compatible
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	//use the OpenGL Core (http://www.opengl.org/wiki/Core_And_Compatibility_in_Contexts)
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	int count;
	GLFWmonitor** monitors = glfwGetMonitors(&count);
	GLFWmonitor* output_monitor = NULL;
	if (count > 1) {
		output_monitor = monitors[1];
		const GLFWvidmode * mode = glfwGetVideoMode(output_monitor);
		WINDOWS_WIDTH = mode->width;
		WINDOWS_HEIGHT = mode->height;
	}
	//create a GLFW windows object
	//g_window = glfwCreateWindow(WINDOWS_WIDTH, WINDOWS_HEIGHT, "Chapter 4 - Texture Mapping", NULL, NULL);
	g_window = glfwCreateWindow(WINDOWS_WIDTH, WINDOWS_HEIGHT,
			"Ray Reality Platform", output_monitor, NULL);
	if (!g_window) {
		fprintf( stderr,
				"Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	//make the context of the specified window current for the calling thread
	glfwMakeContextCurrent(g_window);
	glfwSwapInterval(1);

	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Final to Initialize GLEW\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	//keyboard input callback
	glfwSetInputMode(g_window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetKeyCallback(g_window, key_callback);

}

//TODO: Need clean up once it is functional.
/**
 * General flow of this AR core engine main code
 *
 * There will be a set of core components to be enabled in this demo
 *
 * 1. Tracking Engine (6 DoF) with Intel's SLAM
 * 2. Hand Tracking (Full Hand Pose) with Leap Motion Controller
 * 3. Rendering Engine (Texture and 3D Objects) with GLFW3, Assimp, and OpenGL 4.0 Shaders
 * 4. AI Core (Voice recognition) with Google Speech Recognition APIs.
 * 5. Depth and Scene Tracking with OpenCV and QRCode
 *
 * The engine should be portable amount different inputs such as pose tracking and hand trackings should be decoupled.
 */
int main (int argc, char* argv[])
{

	initGLFW();
	//controller class
	leapM = new LeapMotionController();
	leapM->run();

	rendererM = new RendererManager();
	rendererM->init();

	//AI voice control class
	google_voice = new GoogleVoiceDemo();
	google_voice->init();
	google_voice->run();

	voice_bubble = new VoiceBubble();

	//create 3 bubbles
	bubble_list.push_back(new VoiceBubble);
	bubble_list.push_back(new VoiceBubble);
	bubble_list.push_back(new VoiceBubble);


	//tracking class
	// Handle Ctrl-C
	install_signal_handler();

	//--------------------------------------------------------------------------------------
	// Setup a librealsense context and camera device
	//--------------------------------------------------------------------------------------

	std::unique_ptr<context_interface> context;
	if (argc<2)
	{
		// Create a regular context for streaming live data from the camera
		context.reset(new rs::core::context());
		if(context->get_device_count() == 0)
		{
			cout << "Error: Device is null." << "There are no RealSense devices connected." << endl << "Please connect a RealSense device and restart the application" << endl;
			return -1;
		}
	}
	else
	{
		// Create a playback context if a filename was provided as an argument
		context.reset(new rs::playback::context(argv[1]));
		if(context->get_device_count() == 0)
		{
			cerr<<"error : can't open file" << endl;
			return -1;
		}
	}

	rs::device *device = context->get_device(0);
	//--------------------------------------------------------------------------------------
	// Setup an instance of the SLAM module
	//--------------------------------------------------------------------------------------

	std::unique_ptr<slam> slam(new rs::slam::slam());

	//slam->set_occupancy_map_resolution(0.025);
	//no mapping for now
	slam->set_auto_occupancy_map_building(false);

	//slam->load_occupancy_map("occupancy_map.bin");


	// Register a slam_event_handler (defined above) to receive the output of the SLAM module
	slam_event_handler slamEventHandler;
	slam->register_event_handler(&slamEventHandler);

	//--------------------------------------------------------------------------------------
	// Do some sanity checking
	//--------------------------------------------------------------------------------------

	// Ask the SLAM module what configuration it supports
	video_module_interface::supported_module_config supported_slam_config = {};
	if (slam->query_supported_module_config(0, supported_slam_config) < status_no_error)
	{
		cerr<<"error : failed to query the first supported module configuration" << endl;
		return false;
	}

	// Check to make sure the current device name (ex: Intel RealSense ZR300) is one that the SLAM module supports
	auto device_name = device->get_name();
	auto is_current_device_valid = (strcmp(device_name, supported_slam_config.device_name) == 0);
	if (!is_current_device_valid)
	{
		cerr<<"error : current device is not supported by the current module configuration" << endl;
		return -1;
	}

	// Check to make sure that the current device supports motion data
	rs::source active_sources = get_source_type(supported_slam_config);
	if(!check_motion_sensor_capability_if_required(device, active_sources))
	{
		cerr<<"error : current device does not support motion events" << endl; // If you get this, unplug and reconnect the camera
		return -1;
	}

	//--------------------------------------------------------------------------------------
	// Configure the camera device for SLAM
	//--------------------------------------------------------------------------------------

	configure_camera_for_slam(device, supported_slam_config);
    device->enable_stream(rs::stream::color, 1920, 1080, rs::format::rgb8, 30);


	stream_stats inputStreamStats; // Not used in this example
	set_callback_for_image_stream(device, stream_type::fisheye, slam, inputStreamStats);
	set_callback_for_image_stream(device, stream_type::depth, slam, inputStreamStats);
	set_callback_for_motion_streams(device, slam, inputStreamStats);

	//--------------------------------------------------------------------------------------
	// Set the SLAM configuration
	//--------------------------------------------------------------------------------------

	// Construct the SLAM configuration using a handy helper function
	video_module_interface::actual_module_config slam_config = get_slam_config(device, slam, supported_slam_config);

	if(slam->set_module_config(slam_config) < status_no_error)
	{
		cerr<<"error : failed to set the SLAM configuration" << endl;
		return -1;
	}

	depth_scale = device->get_depth_scale();

	cout << "Depth Scale is set to :" << depth_scale << endl;
	//--------------------------------------------------------------------------------------
	// Setup the map window
	//--------------------------------------------------------------------------------------

	cv::namedWindow(WINDOW_NAME, 0);
	cv::resizeWindow(WINDOW_NAME, WINDOW_WIDTH, WINDOW_HEIGHT);

	//--------------------------------------------------------------------------------------
	// Start streaming data from the camera device
	//--------------------------------------------------------------------------------------

	//rs::core::status status = slam->load_relocalization_map("relocalization_map.bin");
	//slam->force_relocalization_pose(true);

	//if(status == status_no_error){
	//	printf("Loaded Relocalization Map Successfully \n");
	//}else{
	//	printf("Load Relocalization Map Failed \n");
	//}

	cout << endl << "Starting SLAM..." << endl;
	device->start(active_sources);

	//--------------------------------------------------------------------------------------
	// Run until a key is pressed
	//--------------------------------------------------------------------------------------
	cout << "Press Esc key to exit" << endl;


	//double previous_time = glfwGetTime();

	//float counter = 0.0f;
	while(!is_key_pressed() && device->is_streaming())
	{
		int width, height;
		glfwGetFramebufferSize(g_window, &width, &height);

		//perform updates for all elements which requires new texture or alike
		if(drawBackground){
			device->wait_for_frames();
			rendererM->updateBackgroundFrame((unsigned char*)device->get_frame_data(rs::stream::color), 1920, 1080);
		}

    	if(debug_depth){
    		//TODO: Clean up here
			double maxVal = 5000;
			double minVal = 200;
			depth_cv.convertTo(output, CV_8U, 255.0/(maxVal - minVal), -minVal * 255.0/(maxVal - minVal));
			applyColorMap(output, output, COLORMAP_JET);
			rendererM->updateDepthFrame((unsigned char*)output.data, 320, 240);

			//TODO: optimize here
			if(fish_buffer == NULL){
				fish_buffer = (uint8_t *)malloc(sizeof(uint8_t)*FISH_WIDTH*FISH_HEIGHT);
			}
			//let's make a 3 color fish
			rendererM->updateFishEyeFrame(fish_buffer, FISH_WIDTH, FISH_HEIGHT);
    	}
		rendererM->updateBubble(voice_bubble->getBubble());

		//update the hand object
		if(leapM->getHandCount() > 0){
			glm::mat4 model_matrix = glm::mat4(1.0f);
			glm::vec3 hand_pos = leapM->getPalmPosition();
			glm::vec3 hand_angles = leapM->getPalmAngles();
			//printf("Hand Angles %lf %lf %lf\n", hand_angles.x, hand_angles.y, hand_angles.z);
			model_matrix = glm::translate(model_matrix, glm::vec3(-hand_pos.x/1000-0.10, -hand_pos.z/1000+0.02, -hand_pos.y/1000));
			//model_matrix = glm::rotate(model_matrix, -hand_angles.x+glm::pi<float>()*0.25f, glm::vec3(1.0f, 0.0f, 0.0f));
			//model_matrix = glm::rotate(model_matrix, hand_angles.z-glm::pi<float>()*0.5f, glm::vec3(0.0f, 1.0f, 0.0f));
			//model_matrix = glm::rotate(model_matrix, -glm::pi<float>()*0.5f, glm::vec3(0.0f, 0.0f, 1.0f));
			//model_matrix = model_matrix*glm::eulerAngleYXZ(glm::pi<float>()*0.5f, 0.0f, glm::pi<float>());
			//model_matrix = model_matrix*glm::eulerAngleYXZ(0.0f, -hand_angles.x, 0.0f);
			//model_matrix = model_matrix*glm::eulerAngleYXZ(-hand_angles.y+glm::pi<float>()*0.5f, 0.0f, 0.0f);
			//model_matrix = model_matrix*glm::eulerAngleYXZ(0.0f, 0.0f, hand_angles.z+glm::pi<float>()+glm::pi<float>()*0.25f);
			//model_matrix = glm::rotate(model_matrix, glm::pi<float>()+glm::pi<float>()*0.25f, glm::vec3(1.0f, 0.0f, 1.0f));
			//model_matrix = model_matrix*glm::eulerAngleYXZ(-hand_angles.y, 0.0f, 0.0f);
			model_matrix = model_matrix*glm::eulerAngleYXZ(-glm::pi<float>()*0.5f, glm::pi<float>()*0.25f, glm::pi<float>()*1.25f);
			model_matrix = glm::rotate(model_matrix, -hand_angles.z+glm::pi<float>()*0.25f, glm::vec3(1.0f, 0.0f, 0.0f));
			model_matrix = glm::rotate(model_matrix, hand_angles.y+glm::pi<float>()*0.8f, glm::vec3(1.0f, 1.0f, 0.0f));
			//model_matrix = glm::rotate(model_matrix, hand_angles.x, glm::vec3(1.0f, 1.0f, 1.0f));

			model_matrix = glm::scale(model_matrix, glm::vec3(0.30f));

			rendererM->updateHandModel(model_matrix);
			rendererM->setAlphaHand(1.0f);
		}else{
			glm::mat4 model_matrix = glm::mat4(1.0f);
			model_matrix = glm::scale(model_matrix, glm::vec3(0.01f));
			rendererM->updateHandModel(model_matrix);
			rendererM->setAlphaHand(0.0f);
		}
		rendererM->step(width, height); //render the scene

		//cv::imshow("Voice Bubble", voice_bubble->getBubble());
		//cv::waitKey(1);
		//double time_stamp = glfwGetTime();
		//clear the terminal
		//printf( "\033[0m\033[2J\033[H");
		//printf("Timestamp: %lf, diff: %lf, fps: %lf\n", time_stamp, time_stamp-previous_time,1.0/(time_stamp-previous_time));
		//reset timer
		//previous_time = time_stamp;
		if(reset==true){
			slam->restart();
			reset = false;
		}
		glfwSwapBuffers(g_window);
		glfwPollEvents();
	}

	//end game loop -destructions
	slam->unregister_event_handler(&slamEventHandler); // Not strictly necessary, but stops printing pose data to console

	//--------------------------------------------------------------------------------------
	// Stop SLAM module and close camera device
	//--------------------------------------------------------------------------------------

	cout << endl << "Stopping..." << endl;
	slam->flush_resources();
	//device->stop(active_sources);
	device->stop();
	//--------------------------------------------------------------------------------------
	// Save the occupancy map to disk
	//--------------------------------------------------------------------------------------
	//cout << "Saving occupancy map to disk..." << endl;
	//slam->save_occupancy_map("occupancy_map.bin");
	//slam->save_relocalization_map("occupancy_map.bin");

	//disable the Google Voice APIs and terminate the threads
	google_voice->end();

	//disable graphics elements
	rendererM->end();



	if(fish_buffer)
		free(fish_buffer);

	//stop the leapMotion controller
	leapM->end();

	// Close OpenGL window and terminate GLFW
	glfwDestroyWindow(g_window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
