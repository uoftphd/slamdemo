/*
 * GoogleVoiceDemo.h
 *
 *  Created on: Nov 26, 2017
 *      Author: raymond
 */

#ifndef SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_GOOGLEVOICEDEMO_H_
#define SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_GOOGLEVOICEDEMO_H_

#include <grpc++/grpc++.h>

#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <thread>
#include <mutex>

#include "google/cloud/speech/v1/cloud_speech.grpc.pb.h"
#include "VoiceBubble.h"

#include <AL/al.h>
#include <AL/alc.h>
#include "AL/alext.h"
#include <AL/alut.h>

#include <unistd.h>
using namespace std;


using google::cloud::speech::v1::RecognitionConfig;
using google::cloud::speech::v1::Speech;
using google::cloud::speech::v1::StreamingRecognizeRequest;
using google::cloud::speech::v1::StreamingRecognizeResponse;
using google::cloud::speech::v1::StreamingRecognitionConfig;

class GoogleVoiceDemo {
private:
	std::thread microphone_thread;
	std::thread stream_thread;
	std::thread main_thread;

	void MicrophoneThreadMain(
            grpc::ClientReaderWriterInterface<StreamingRecognizeRequest,
            StreamingRecognizeResponse>* streamer);
	void runStreamResponse(grpc::ClientReaderWriterInterface<StreamingRecognizeRequest,
            StreamingRecognizeResponse>* streamer);


	std::shared_ptr<grpc::ChannelCredentials> creds;
	std::shared_ptr<grpc::Channel> channel;
	std::unique_ptr<Speech::Stub> speech;

	// Parse command line arguments.
	StreamingRecognizeRequest *request;
	StreamingRecognitionConfig *streaming_config;
	RecognitionConfig *config;
	int code;
	int exit;


	std::unique_ptr<grpc::ClientReaderWriter<StreamingRecognizeRequest,StreamingRecognizeResponse>>
	streamer;
	grpc::ClientContext *context;
    ALCdevice *alDevice = NULL;
    VoiceBubble *bubble;
public:
	GoogleVoiceDemo();
	void init();
	void run();
	void stop();
	void end();
	void extract(VoiceBubble *b);
	void updateStatus(int code);
	void playWAVSound(string file_name);
	int getStatus();
	void manager();
	virtual ~GoogleVoiceDemo();
};

#endif /* SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_GOOGLEVOICEDEMO_H_ */
