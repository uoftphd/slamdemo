/*
 * GoogleVoiceDemo.cpp
 *
 *  Created on: Nov 26, 2017
 *      Author: raymond
 */

#include "GoogleVoiceDemo.h"

using google::cloud::speech::v1::RecognitionConfig;
using google::cloud::speech::v1::Speech;
using google::cloud::speech::v1::StreamingRecognizeRequest;
using google::cloud::speech::v1::StreamingRecognizeResponse;

std::mutex mu;
std::mutex mu_exit;


GoogleVoiceDemo::GoogleVoiceDemo() {
	// TODO Auto-generated constructor stub

}
void GoogleVoiceDemo::updateStatus(int i_code){
	mu.lock();
	code = i_code;
	//std::cout << msg << ":" << id << std::endl;
	mu.unlock();
}
int GoogleVoiceDemo::getStatus(){
	int local_code = 0;
	mu.lock();
	local_code = code;
	mu.unlock();

	return local_code;
}
// Write the audio in 64k chunks at a time, simulating audio content arriving
// from a microphone.
void GoogleVoiceDemo::MicrophoneThreadMain(
                grpc::ClientReaderWriterInterface<StreamingRecognizeRequest,
                StreamingRecognizeResponse>* l_streamer) {
//		l_streamer->WritesDone();
//		return;

		cout << "Capture Thread Started: " << endl;
	    const ALchar *pDeviceList = alcGetString(NULL, ALC_CAPTURE_DEVICE_SPECIFIER);
	    if (pDeviceList)
	    {
	        printf("\nAvailable Capture Devices are:-\n");

	        while (*pDeviceList)
	        {
	        	printf("%s\n", pDeviceList);
	            pDeviceList += strlen(pDeviceList) + 1;
	        }
	    }
	    const int SRATE = 16000, SSIZE = 32768;
	    cout << "Opening Capture Device" << endl;
	    alDevice = alcCaptureOpenDevice(NULL, SRATE, AL_FORMAT_MONO16, SSIZE);
	    if(!alDevice)
	    {
	            cout << "Dead" << endl;
	            return;
	    }
	    printf("Opened \"%s\"\n", alcGetString(
	                            alDevice, ALC_CAPTURE_DEVICE_SPECIFIER
	                            ));
	    alcCaptureStart(alDevice);

        ALint count = 0 ;
        ALsizei bufferSize=0;
        //short dint = 0;
        size_t frameSize = 2; //1*32/8; //1 channel, 32 bits
        // Get list of available Capture Devices

        cout << "Capture Started" << getStatus() << endl;
        StreamingRecognizeRequest request;
        bufferSize = 64*1024+1024*2; //with some padding
        //larger buffer to before sending
        ALbyte *data = (ALbyte *)calloc(bufferSize, sizeof(ALbyte)*frameSize);
        size_t total_count = 0;
        int seconds = 0;
        bool is_running = true;
        cout << "Running the thread for streaming: " << getStatus() << endl;
        while (getStatus()==1) {
                alcGetIntegerv(alDevice, ALC_CAPTURE_SAMPLES, 1, &count);
                if(count < 1)
                {
                        std::this_thread::sleep_for(std::chrono::milliseconds(1));
                        continue;
                }
                alcCaptureSamples(alDevice, data+total_count*frameSize, count);
                total_count += count;
                // And write the chunk to the stream.
                if(total_count*frameSize >= 32*1024){
                        request.set_audio_content(data, total_count*frameSize);
                        bubble->appendAudioBuffer(data, total_count*frameSize);
                        std::cout << "Sending " <<  total_count*frameSize << "bytes." << std::endl;
                        l_streamer->Write(request);
                        total_count = 0;
                        if(seconds>8){
                        	std::cout << "Finished 10 seconds" << endl;
                        	l_streamer->WritesDone();
							updateStatus(0);
							is_running=false;
						}
                        seconds++;
                }
        }
        if(is_running)
        	l_streamer->WritesDone(); //just to ensure we are done
    	std::cout << "Finished and Quitting" << endl;
        free(data);
        alcCaptureStop(alDevice);
        alcCaptureCloseDevice(alDevice);
    	std::cout << "Finished and Quitted" << endl;
}
void GoogleVoiceDemo::playWAVSound(string file_name){
    ALuint buffer;
    ALuint source;
    ALint state;

    // Initialize the environment
    alutInit(0, NULL);

    // Capture errors
    alGetError();

    // Load pcm data into buffer
    buffer = alutCreateBufferFromFile(file_name.c_str());

    // Create sound source (use buffer to fill source)
    alGenSources(1, &source);
    alSourcei(source, AL_BUFFER, buffer);

    // Play
    alSourcePlay(source);

    // Wait for the song to complete
    do {
        alGetSourcei(source, AL_SOURCE_STATE, &state);
    } while (state == AL_PLAYING);

    // Clean up sources and buffers
    alDeleteSources(1, &source);
    alDeleteBuffers(1, &buffer);

    // Exit everything
    alutExit();
}
//non-blocking thread it
void GoogleVoiceDemo::init(){
	creds = grpc::GoogleDefaultCredentials();
	channel = grpc::CreateChannel("speech.googleapis.com", creds);
	speech = Speech::NewStub(channel);

	// Parse command line arguments.
	request = new StreamingRecognizeRequest();

	streaming_config = request->mutable_streaming_config();

	config = streaming_config->mutable_config();
	config->set_language_code("en"); //can work with any languages :D
	config->set_sample_rate_hertz(16000);  // Default sample rate.
	config->set_encoding(RecognitionConfig::LINEAR16);

	updateStatus(0);
	exit = 0;
}
void GoogleVoiceDemo::run(){
	main_thread = std::thread(&GoogleVoiceDemo::manager, this);
	updateStatus(0);
}
void GoogleVoiceDemo::runStreamResponse(grpc::ClientReaderWriterInterface<StreamingRecognizeRequest,
        StreamingRecognizeResponse>* l_streamer){
	// Read responses.
	playWAVSound("data/start.wav");
	cout << "Streamer Response Thread Started..." << endl;
	StreamingRecognizeResponse response;
	while (l_streamer->Read(&response)) {  // Returns false when no more to read.
			// Dump the transcript of all the results.
			for (int r = 0; r < response.results_size(); ++r) {
					auto result = response.results(r);
					std::cout << "Result stability: " << result.stability() << std::endl;
					for (int a = 0; a < result.alternatives_size(); ++a) {
							auto alternative = result.alternatives(a);
							std::cout << alternative.confidence() << "\t"
									<< alternative.transcript() << std::endl;
							bubble->appendText(alternative.transcript(), alternative.confidence());
					}
			}
	}
	cout << "Streamer is done..." << endl;
	grpc::Status status = l_streamer->Finish();
	cout << "Streamer is Now Finished..."<<status.error_message()<< endl;

	if (!status.ok()) {
			// Report the RPC failure.
			std::cerr << status.error_message() << std::endl;
			return;
	}

	delete(context);
	cout << "Streamer is Now Exited..."<<status.error_message()<< endl;
	playWAVSound("data/end.wav");
}
void GoogleVoiceDemo::manager(){
	while(exit == 0){
		if(getStatus()==1){
			cout<<"Starting Speech ..." << endl;
			// Begin a stream.
			context = new grpc::ClientContext();
			streamer = speech->StreamingRecognize(context);
			// Write the first request, containing the config only.
			streaming_config->set_interim_results(false);
			streamer->Write(*request);

			cout<<"Starting Streaming Thread..." << endl;

			//we got works todo!
			// The microphone thread writes the audio content.
			microphone_thread = std::thread(&GoogleVoiceDemo::MicrophoneThreadMain, this, streamer.get());
			//This will draw on screen
			stream_thread = std::thread(&GoogleVoiceDemo::runStreamResponse, this, streamer.get());
			//GoogleVoiceDemo::runStreamResponse(streamer.get(), bubble);

			microphone_thread.join();
			stream_thread.join();
			updateStatus(0); //all done and good
		}
        std::this_thread::sleep_for(std::chrono::milliseconds(3));
	}
	cout<<"Manager Quitting..." <<endl;
}
void GoogleVoiceDemo::extract(VoiceBubble *bubble){
	if(getStatus()==1){
		cout<<"Already running, please wait until the last one is done" << endl;
		return;
	}
	this->bubble = bubble;
	updateStatus(1); //this will make the main worker thread starts
}
void GoogleVoiceDemo::stop(){
	updateStatus(0);
	//stream_thread.join();
}
void GoogleVoiceDemo::end(){
	cout<<"Ending the GoogleVoiceDemo Threads..." <<endl;
	updateStatus(0);
	exit = 1;
	cout<<"Updated Status..." <<endl;
	main_thread.join();
	cout<<"Main Thread Quitted..." <<endl;

	//now wait for all threads to end with stop function
	//free memories
}

GoogleVoiceDemo::~GoogleVoiceDemo() {
	// TODO Auto-generated destructor stub
}

