/*
 * MPV.h
 *
 *  Created on: Nov 30, 2017
 *      Author: raymond
 */

#ifndef SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_MVP_H_
#define SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_MVP_H_
//this handles the model, view, and projection matrix
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include "common.h"

class MVP {
private:
	//model matrix
	glm::mat4 model_m;

	//view matrix
	glm::mat4 view_m;

	//pose from the camera
	glm::mat4 pose_rotation;
	glm::vec3 pose_translation;

	//the final MVP matrix
	glm::mat4 mvp_m;

	//for projection matrix
	glm::mat4 projective_m;
	float fov;
	float width;
	float height;

	//don't touch them for now / internal use only
	float far_plane;
	float near_plane;

public:
	//void setModelMatrix(glm::mat4 model_matrix);
	//void setViewMatrix(glm::mat4 view_matrix);
	//void setProjectiveMatrix(glm::mat4 projective_matrix);
	void setProjectiveMatrixParam(float fov, float width, float height);

	//set the camera pose and will affect the eye position
	void setPoseRotationMatrix(glm::mat4 pose);
	void setPoseTranslationMatrix(glm::vec3 translation);

	void setModelMatrix(glm::mat4 model);

	//calculate the MVP on the fly and make sure it is correct
	glm::mat4 computeMVP();
	//or just get the MVP without any computations
	glm::mat4 getMVP();
	MVP();
	MVP(float i_fov, float i_near_plane, float i_far_plane);

	virtual ~MVP();
};

#endif /* SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_MVP_H_ */
