/*
 * RendererManager.h
 *
 *  Created on: Nov 27, 2017
 *      Author: raymond
 */

#ifndef SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_RENDERERMANAGER_H_
#define SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_RENDERERMANAGER_H_

//GLM library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

//OpenGL libraries
#include "shader.hpp"
#include "texture.hpp"
#include "controls.hpp"

#include "PlaneRenderer.h"
#include "OBJRenderer.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class RendererManager {
private:
	float rotateY;
	float rotateX;

	bool reset_pose;
	bool save_pose;
	bool debug;

	PlaneRenderer *background_plane;
	PlaneRenderer *bubble_renderer;

	//these are debug panels
	PlaneRenderer *depth_plane;
	PlaneRenderer *fish_plane;

	OBJRenderer *obj_renderer;
	OBJRenderer *hand_renderer;

public:
	//hacks
    void KeyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

	RendererManager();

	void init();
	void end();

	void initPlane();
	void initOBJ();

	void step(int width, int height);

	//some update loops
	void updateBackgroundFrame(unsigned char *frame, int width, int height);
	void updateDepthFrame(unsigned char *frame, int width, int height);
	void updateBubble(cv::Mat bubbleTexture);
	void updateFishEyeFrame(unsigned char *frame, int width, int height);

	//controls for the updates
	void updatePose(glm::mat4 pose);
	void updateTranslation(glm::vec3 translation);
	void updateBubbleModel(glm::mat4 pose);
	void setAlpha (float alpha);
	void setAlphaHand (float alpha);
	void updateHandModel(glm::mat4 pose);

	void setDebug(bool flag);
	virtual ~RendererManager();
};

#endif /* SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_RENDERERMANAGER_H_ */
