/*
 * OBJRenderer.h
 *
 *  Created on: Nov 28, 2017
 *      Author: raymond
 */

#ifndef SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_OBJRENDERER_H_
#define SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_OBJRENDERER_H_
//GLM library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string>

#include "texture.hpp"
#include "shader.hpp"
#include "ObjLoader.h"

#include "MVP.h"

class OBJRenderer {
private:

	//Shader Program for 3D Objects
	GLuint program_id;
	GLuint matrix_id;
	GLuint alpha_id;
	GLint attribute_vertex;
	GLuint vertex_array_id;
	GLuint vertex_buffer;

	//OBJ buffers
	unsigned int numVerticesScene;
	GLfloat *g_vertex_buffer_data;

	//for handling states
	float alpha_v;

	//store the MVP matrices and operations here
	MVP mvp;

public:
	OBJRenderer();

	//these are common interfaces
	void init(string filename);
	void end();
	void render(int width, int height);

	void updatePose(glm::mat4 pose);
	void updateTranslation(glm::vec3 translation);
	void updateModel(glm::mat4 model);

	void setAlpha (float alpha);

	virtual ~OBJRenderer();
};

#endif /* SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_OBJRENDERER_H_ */
