/*
 * PlaneRenderer.cpp
 *
 *  Created on: Nov 28, 2017
 *      Author: raymond
 */

#include "PlaneRenderer.h"

void PlaneRenderer::end() {
	glDisableVertexAttribArray(attribute_vertex_texture);
	glDisableVertexAttribArray(attribute_uv);

	// // Cleanup VBO and shader
	glDeleteBuffers(1, &vertex_buffer_texture);
	glDeleteBuffers(1, &uv_buffer);
	glDeleteProgram(program_texture);
	glDeleteTextures(1, &texture_id);
	glDeleteVertexArrays(1, &vertex_array_id_texture);
	free(frame);
}

void PlaneRenderer::updateModel(glm::mat4 model_pose){
	mvp.setModelMatrix(model_pose);
}
void PlaneRenderer::init(int width, int height, GLenum format) {
	this->frame_width = width;
	this->frame_height = height;
	this->frame_format = format;

	if (format == GL_RGBA) {
		this->frame_channel = 4;
	} else if (format == GL_RGB) {
		this->frame_channel = 3;
	} else if (format == GL_LUMINANCE){
		this->frame_channel = 1;
	}
	else{
		this->frame_channel = 4;// just in case
	}

	aspect_ratio = 1.0f;
	z_offset = -1.0f;

	// //our vertices, just fill screen
	float tmp_g_vertex_buffer_data_texture[] = { aspect_ratio, aspect_ratio,
			z_offset, -aspect_ratio, aspect_ratio, z_offset, -aspect_ratio,
			-aspect_ratio, z_offset, aspect_ratio, aspect_ratio, z_offset,
			-aspect_ratio, -aspect_ratio, z_offset, aspect_ratio, -aspect_ratio,
			z_offset, };
	memcpy(g_vertex_buffer_data_texture, tmp_g_vertex_buffer_data_texture,
			sizeof(float) * 18);
	// //UV map for the vertices
	float tmp_g_uv_buffer_data[] = { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 1.0f, 1.0f };
	memcpy(g_uv_buffer_data, tmp_g_uv_buffer_data, sizeof(float) * 12);

	//Shader program for texture mapping
	program_texture = 0;
	texture_id = 0;
	matrix_id_texture = 0;
	texture_sampler_id = 0;
	attribute_vertex_texture = 0;
	attribute_uv = 0;
	vertex_buffer_texture = 0;
	uv_buffer = 0;
	vertex_array_id_texture = 0;
	//cull triangles which normal is not towards the camera - this can be used to
	//avoid rendering back-facing surfaces if we are rendering 3D objects.
	//glEnable(GL_CULL_FACE);

	//http://www.opengl.org/wiki/Vertex_Specification
	//create and compile our GLSL program from the shaders
	program_texture = LoadShaders("data/transform.vert", "data/texture.frag");
	texture_id = 0;
	frame = (unsigned char*) malloc(
			sizeof(unsigned char) * frame_width * frame_height * 4); //just use 4 all the time
	texture_id = initializeTexture(frame, frame_width, frame_height); //just push some random data in
	printf("Initialized Texture (id=%d) of size %d x %d \n", texture_id, frame_width, frame_height);

	// //get the location for our "MVP" uniform variable
	matrix_id_texture = glGetUniformLocation(program_texture, "MVP");

	// //get the location for our "textureSampler" uniform variable
	texture_sampler_id = glGetUniformLocation(program_texture,
			"textureSampler");

	// //get the location for the attribute variables
	attribute_vertex_texture = glGetAttribLocation(program_texture,
			"vertexPosition_modelspace");
	attribute_uv = glGetAttribLocation(program_texture, "vertexUV");

	// //generate the Vertex Array Object (Depedency: GLEW)
	// //http://www.arcsynthesis.org/gltut/Positioning/Tutorial%2005.html
	// //http://www.informit.com/articles/article.aspx?p=1377833&seqNum=8
	glGenVertexArrays(1, &vertex_array_id_texture);
	glBindVertexArray(vertex_array_id_texture);

	// //initialize the vertex buffer memory (similar to malloc)
	// //https://www.khronos.org/opengles/sdk/docs/man/xhtml/glGenBuffers.xml
	glGenBuffers(1, &vertex_buffer_texture);
	// //https://www.khronos.org/opengles/sdk/docs/man/xhtml/glBindBuffer.xml
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_texture);
	// //https://www.opengl.org/sdk/docs/man3/xhtml/glBufferData.xml
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data_texture),
			g_vertex_buffer_data_texture, GL_STATIC_DRAW);

	// //initialize the UV buffer memory
	// //generate the array (like malloc)
	glGenBuffers(1, &uv_buffer);
	// //bind the array buffer to that UV
	glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_uv_buffer_data), g_uv_buffer_data,
	GL_STATIC_DRAW);

	// //uses our shader
//	glUseProgram(program_texture);
//
//	 //binds our texture in Texture Unit 0
//	glActiveTexture(GL_TEXTURE0);
//	glBindTexture(GL_TEXTURE_2D, texture_id);
//	glUniform1i(texture_sampler_id, 0);
//
//	// //1st attribute buffer : vertices for position
//	glEnableVertexAttribArray(attribute_vertex_texture);
//	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_texture);
//	glVertexAttribPointer(attribute_vertex_texture, 3, GL_FLOAT, GL_FALSE, 0,
//			(void*) 0);
//
//	// //2nd attribute buffer : UVs mapping
//	glEnableVertexAttribArray(attribute_uv);
//	glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);
//	glVertexAttribPointer(attribute_uv, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	fullscreen=false;
}
PlaneRenderer::PlaneRenderer() {
	// TODO Auto-generated constructor stub
}

void PlaneRenderer::setFullScreen(bool flag){
	this->fullscreen = flag;
}
void PlaneRenderer::render(int width, int height) {
	//render the background image
	glUseProgram(program_texture);

	//glBindVertexArray(vertex_array_id_texture);
	//glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_texture);
	//glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);


	// //binds our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glUniform1i(texture_sampler_id, 0);


	// //1st attribute buffer : vertices for position
	glEnableVertexAttribArray(attribute_vertex_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_texture);
	glVertexAttribPointer(attribute_vertex_texture, 3, GL_FLOAT, GL_FALSE, 0,
			(void*) 0);

	// //2nd attribute buffer : UVs mapping
	glEnableVertexAttribArray(attribute_uv);
	glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);
	glVertexAttribPointer(attribute_uv, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);

	glm::mat4 mvp_texture;
	if(fullscreen)
		mvp_texture=glm::mat4(1.0f);
	else{
		mvp.setProjectiveMatrixParam(FOV, width, height);
		//glm::mat4 model_matrix = glm::mat4(1.0f);
		//model_matrix = glm::translate(model_matrix, glm::vec3(0.0, 0.0, -0.25));
		//model_matrix = glm::translate(model_matrix, glm::vec3(0.0, 0.0, -0.50));
		//model_matrix=glm::scale(model_matrix, glm::vec3(0.1f, 0.1f, 0.1f));
		//mvp.setModelMatrix(model_matrix);

		mvp_texture = mvp.computeMVP();//glm::mat4(1.0); //projection_matrix_texture * view_matrix_texture *  model_matrix_texture;
	}
	glUniformMatrix4fv(matrix_id_texture, 1, GL_FALSE, &mvp_texture[0][0]);
	updateTexture(frame, frame_width, frame_height, frame_format);
//	if (debug) {
//		//cout << depth_info.width << "x" << depth_info.height << " : " << (depth_info.format == pixel_format::z16) << endl;
//		Mat depth_cv(Size(320, 240), CV_16UC1, (void*) depth_buffer,
//				Mat::AUTO_STEP);
//		Mat output;
//		double maxVal = 5000;
//		double minVal = 200;
//		depth_cv.convertTo(output, CV_8U, 255.0 / (maxVal - minVal),
//				-minVal * 255.0 / (maxVal - minVal));
//		applyColorMap(output, output, COLORMAP_JET);
//		updateTexture((unsigned char*) output.data, 320, 240, GL_BGR);
//	}

	glDrawArrays(GL_TRIANGLES, 0, 6); //draw the square
}
void PlaneRenderer::updateFrame(unsigned char *input, int width, int height,
		int num_channel) {
	memcpy(frame, input, sizeof(unsigned char) * width * height * num_channel);
}

void PlaneRenderer::updatePose(glm::mat4 pose_matrix){
	mvp.setPoseRotationMatrix(pose_matrix);
}
void PlaneRenderer::updateTranslate(glm::vec3 translation){
	mvp.setPoseTranslationMatrix(translation);
}

PlaneRenderer::~PlaneRenderer() {
	// TODO Auto-generated destructor stub
}

