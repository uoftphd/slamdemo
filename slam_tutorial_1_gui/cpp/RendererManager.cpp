/*
 * RendererManager.cpp
 *
 *  Created on: Nov 27, 2017
 *      Author: raymond
 */

#include "RendererManager.h"
//========================================================================
// Handle key strokes
//========================================================================
RendererManager::RendererManager() {

}

void RendererManager::end() {
	background_plane->end();
	obj_renderer->end();
	bubble_renderer->end();
	depth_plane->end();
	hand_renderer->end();
}

void RendererManager::setDebug(bool flag){
	debug = flag;
}

void RendererManager::init() {
	rotateY = 0.0f;
	rotateX = 0.0f;

	reset_pose = false;
	save_pose = false;
	debug = false;


	//initilize all objects here
	background_plane = new PlaneRenderer();
	background_plane->init(1920, 1080, GL_RGB);
	background_plane->setFullScreen(true);

	//Dragon demo
	obj_renderer = new OBJRenderer();
	obj_renderer->init("data/dragon.obj");
	glm::mat4 model_matrix = glm::mat4(1.0);
	//ORDER MATTERS
	//model_matrix = glm::scale(model_matrix, glm::vec3(0.1f));
	model_matrix = glm::translate(model_matrix, glm::vec3(-0.05, -0.07, -0.4));
	model_matrix = glm::rotate(model_matrix, glm::pi<float>() * 0.5f,
			glm::vec3(-1.0f, 0.0f, 0.0f));
	model_matrix = glm::rotate(model_matrix, glm::pi<float>() * 0.25f,
			glm::vec3(0.0f, 0.0f, 1.0f));
	model_matrix = glm::scale(model_matrix, glm::vec3(0.5f));
	//model_matrix = glm::translate(model_matrix, glm::vec3(0.1, 0.0, 0.0));
	obj_renderer->updateModel(model_matrix);

	//Dragon demo
	hand_renderer = new OBJRenderer();
	hand_renderer->init("data/hand.obj");
	model_matrix = glm::mat4(1.0);
	//ORDER MATTERS
	//model_matrix = glm::scale(model_matrix, glm::vec3(0.1f));
	model_matrix = glm::translate(model_matrix, glm::vec3(0.0f, 0.0f, 0.0f));
	//model_matrix = glm::rotate(model_matrix, glm::pi<float>() * 0.5f,
	//		glm::vec3(-1.0f, 0.0f, 0.0f));
	//model_matrix = glm::rotate(model_matrix, glm::pi<float>() * 0.25f,
	//		glm::vec3(0.0f, 0.0f, 1.0f));
	model_matrix = glm::scale(model_matrix, glm::vec3(0.1f));
	//model_matrix = glm::translate(model_matrix, glm::vec3(0.1, 0.0, 0.0));
	hand_renderer->updateModel(model_matrix);
	hand_renderer->updatePose(glm::eulerAngleXYZ(0.0f, 0.0f, 0.0f));
	hand_renderer->updateTranslation(glm::vec3(0.0f));
	hand_renderer->setAlpha(0.25f);


	//create the object and also present the origin position for the texture
	bubble_renderer = new PlaneRenderer();
	bubble_renderer->init(1280, 960, GL_RGBA);
	model_matrix = glm::mat4(1.0f);
	model_matrix = glm::translate(model_matrix, glm::vec3(0.0, 0.05, -0.4));
	model_matrix=glm::scale(model_matrix, glm::vec3(0.1f, 0.1f, 0.1f));
	bubble_renderer->updateModel(model_matrix);


	depth_plane = new PlaneRenderer();
	depth_plane->init(320, 240, GL_BGR);
	//set the model matrix
	glm::mat4 model_matrix_depth = glm::mat4(1.0f);
	model_matrix_depth = glm::translate(model_matrix_depth, glm::vec3(0.195, -0.16, -0.5));
	model_matrix_depth=glm::scale(model_matrix_depth, glm::vec3(0.05*1.0, 0.05*3.0f/4.0f, 0.05*1.0f));//16:9 aspect
	depth_plane->updateModel(model_matrix_depth);
	//background_plane->setFullScreen(true);

	fish_plane = new PlaneRenderer();
	fish_plane->init(640, 480, GL_LUMINANCE);
	//set the model matrix
	model_matrix_depth = glm::mat4(1.0f);
	model_matrix_depth = glm::translate(model_matrix_depth, glm::vec3(0.1, -0.16, -0.5));
	model_matrix_depth=glm::scale(model_matrix_depth, glm::vec3(0.05*1.0, 0.05*3.0f/4.0f, 0.05*1.0f));//16:9 aspect
	fish_plane->updateModel(model_matrix_depth);

	//black background


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPointSize(4.0f);
}

void RendererManager::updateBackgroundFrame(unsigned char *input, int width,
		int height) {
	background_plane->updateFrame(input, width, height, 3);
}

void RendererManager::updateBubble(Mat bubbleTexture) {
	bubble_renderer->updateFrame(bubbleTexture.data, bubbleTexture.rows, bubbleTexture.cols, 4);
}
void RendererManager::updateBubbleModel(glm::mat4 pose) {
	bubble_renderer->updateModel(pose);
}
void RendererManager::updateDepthFrame(unsigned char *input, int width,
		int height) {
	depth_plane->updateFrame(input, width, height, 3);
}
void RendererManager::updateFishEyeFrame(unsigned char *input, int width, int height) {
	fish_plane->updateFrame(input, width, height, 1);
}

void RendererManager::updatePose(glm::mat4 pose) {
	//update the pose of all objects //if needed
	obj_renderer->updatePose(pose);
	bubble_renderer->updatePose(pose);
	//hand_renderer->updatePose(pose);
}

void RendererManager::updateHandModel(glm::mat4 pose){
	hand_renderer->updateModel(pose);
}

void RendererManager::updateTranslation(glm::vec3 translation) {
	obj_renderer->updateTranslation(translation);
	bubble_renderer->updateTranslate(translation);
	//hand_renderer->updateTranslation(translation);
}

void RendererManager::setAlpha(float alpha) {
	obj_renderer->setAlpha(alpha);
}

void RendererManager::setAlphaHand(float alpha){
	hand_renderer->setAlpha(alpha);
}

void RendererManager::step(int width, int height) {
	//clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	/*
	 * You are passing the window size, which is in screen coordinates,
	 * to glViewport, which works with pixels. On OS X with a Retina display,
	 * and possibly on other platforms in the future, screen coordinates and
	 * pixels do not map 1:1. Use the framebuffer size, which is in pixels,
	 * instead of the window size. See the Window handling guide for details.
	 */

	//now render the 3D elements on top of the background
	glViewport(0, 0, width, height);

	//computes the MVP matrix from keyboard and mouse input
	//computeViewProjectionMatrices(g_window);
	glDisable(GL_DEPTH_TEST);
	//ORDER matters here
	background_plane->render(width, height);

	if(debug){
		depth_plane->render(width, height);
		fish_plane->render(width, height);
	}
	glEnable(GL_DEPTH_TEST);

	obj_renderer->render(width, height);
	hand_renderer->render(width, height);
	bubble_renderer->render(width, height);
}
RendererManager::~RendererManager() {
	// TODO Auto-generated destructor stub
}

