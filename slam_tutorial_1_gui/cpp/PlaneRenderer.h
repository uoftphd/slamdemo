/*
 * PlaneRenderer.h
 *
 *  Created on: Nov 28, 2017
 *      Author: raymond
 */

#ifndef SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_PLANERENDERER_H_
#define SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_PLANERENDERER_H_
//GLM library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/transform.hpp>

//GLWF library
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string>

#include "texture.hpp"
#include "shader.hpp"

#include "MVP.h"

class PlaneRenderer {
private:
	float aspect_ratio;
	float z_offset;

	// //our vertices, just fill screen
	GLfloat g_vertex_buffer_data_texture[18];
	// //UV map for the vertices
	GLfloat g_uv_buffer_data[12];

	//Shader program for texture mapping the background image
	GLuint program_texture;
	GLuint texture_id;
	GLuint matrix_id_texture;
	GLuint texture_sampler_id;
	GLint attribute_vertex_texture, attribute_uv;
	GLuint vertex_buffer_texture;
	GLuint uv_buffer;
	GLuint vertex_array_id_texture;
	unsigned char *frame;
	GLenum texture_unit;
	MVP mvp;
	bool fullscreen;

public:
	int frame_width;
	int frame_height;
	int frame_channel;
	GLenum frame_format;
	PlaneRenderer();
	PlaneRenderer(GLenum t_unit);

	//these should be common interfaces
	void render(int width, int height);
	void init(int width, int height, GLenum format);
	void end();

	//compute the pose
	void updateModel(glm::mat4 model_matrix);

	//update the pose matrix
	void updatePose(glm::mat4 pose_matrix);
	void updateTranslate(glm::vec3 translation);

	void updateFrame(unsigned char *frame, int width, int height, int num_channel);

	void setFullScreen(bool flag);
	virtual ~PlaneRenderer();
};

#endif /* SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_PLANERENDERER_H_ */
