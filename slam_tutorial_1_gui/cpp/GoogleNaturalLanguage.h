/*
 * GoogleNaturalLanguage.h
 *
 *  Created on: Jan 8, 2018
 *      Author: raymond
 */

#ifndef SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_GOOGLENATURALLANGUAGE_H_
#define SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_GOOGLENATURALLANGUAGE_H_

#include "common.h"
#include <string>
#include <Python.h>

class GoogleNaturalLanguage {
public:
	GoogleNaturalLanguage();
	virtual ~GoogleNaturalLanguage();
	void call(string input);
};

#endif /* SUBPROJECTS__SLAM_TUTORIAL_1_GUI_CPP_GOOGLENATURALLANGUAGE_H_ */
