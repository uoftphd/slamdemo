/*
 * VoiceBubble.cpp
 *
 *  Created on: Nov 27, 2017
 *      Author: raymond
 */

#include "VoiceBubble.h"

VoiceBubble::VoiceBubble() {
	// TODO Auto-generated constructor stub
	bubbleTexture = cv::imread("data/voicebubble.png", cv::IMREAD_UNCHANGED);
	defaultTexture = bubbleTexture.clone();
	//bubbleTexture.convertTo(bubbleTexture, CV_8UC4);
	cv::cvtColor(bubbleTexture, bubbleTexture, CV_BGRA2RGBA);
	voice_size = 64*1024*10; //640 kb for the audio for now
	voice_data = (ALbyte *)calloc(voice_size, sizeof(ALbyte));
	current_size = 0; //zero count
	position = glm::vec3(0.0f);
	pose = glm::mat4(1.0f);
}

VoiceBubble::~VoiceBubble() {
	// TODO Auto-generated destructor stub
}
//void VoiceBubble::triggerVoice(){
//	//if we have concurrent thread running, wait until it is done.
//
//
//}
void VoiceBubble::appendText(std::string input_text, float confidence){
	this->transcribed_text.push_back(input_text);
	this->transcribed_confidence.push_back(confidence);
	this->drawText();
}

void VoiceBubble::clearText(){
	this->transcribed_text.clear();
	this->transcribed_confidence.clear();
	this->drawText();
}
void VoiceBubble::drawText(){
	//overwrite the bubble, empty it out basically
	defaultTexture.copyTo(bubbleTexture);

	//go through the list
	int j = 0;
    for(unsigned int i=0; i<transcribed_text.size(); i++) {
    	string input_text = transcribed_text.at(i);
    	float confidence = transcribed_confidence.at(i);

		int font_face = cv::FONT_HERSHEY_SIMPLEX;
		double font_scale = 2;
		int thickness = 4;
		//wrap text
		std::string text_chunk;
		int x_offset = 40;
		int y_offset = 120;
		int y_height = 62;
		unsigned int width_limit = 36;
		while(input_text.length()>width_limit){
			cv::Point pos_i(x_offset, y_offset+y_height*j);
			if(input_text.length()>width_limit){
				text_chunk=input_text.substr(0, width_limit);
			}
			input_text=input_text.substr(width_limit);
			//add a hypen we broke the word
			if(input_text.compare(0, 1, " ")){
				text_chunk = text_chunk+ "-";
			}

			cv::putText(bubbleTexture, text_chunk, pos_i, font_face, font_scale, cv::Scalar(255*(1-confidence), 255*(confidence), 255*(1-confidence), 255), thickness, 8);
			j++;
		}
		cv::Point pos(x_offset, y_offset+y_height*j);
		cv::putText(bubbleTexture, input_text, pos, font_face, font_scale, cv::Scalar(255*(1-confidence), 255*(confidence), 255*(1-confidence), 255), thickness, 8);
		j++;
    }
}
//size is counted in bytes
//data should be in bytes as well
void VoiceBubble::appendAudioBuffer(ALbyte *data, std::size_t size){
	//if memory overflow due to the append
	if(size+current_size>voice_size){
		ALbyte *tmp = (ALbyte *)calloc(voice_size*2, sizeof(ALbyte));
		voice_size = voice_size*2;
		memcpy(tmp, voice_data, current_size*sizeof(ALbyte));
		free(voice_data);
		voice_data = tmp;
		printf("Created more memory for Audio Data\n");
	}

	//move the pointer to the new position before copy
	memcpy(voice_data+current_size, data, size*sizeof(ALbyte));
	current_size += size;

	//printf("Copied data... %lu %lu %\n", current_size, voice_size);
}

cv::Mat VoiceBubble::getBubble(){
	return bubbleTexture;
}
